package main.java.org.valor.pom.schools.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.POMListener;
import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Progression;
import net.minecraft.server.v1_11_R1.EnumParticle;
import net.minecraft.server.v1_11_R1.Packet;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Lightning extends Progression {

	private static final double maxHeight = 20;
	private static final int chargeTime = 5;
	private static final int pauseTime = chargeTime + 20;
	private static final double range = 20;
	private static final double dParticle = .5;
	private static final Vector down = new Vector(0, -1, 0);

	private final List<Packet<?>> particlePackets = new ArrayList<>();
	private final List<Location> particleLocations = new ArrayList<>();
	private final Location targetLocation;
	private final int particlesPerTick;
	private final Player player;
	private final ParagonsOfMagic plugin;
	private final World world;

	private int i = 0;
	private LightningStrike lightning;

	public Lightning(POMPlayer source) {
		this.isAlive = true;
		this.player = source.getPlayer();

		LivingEntity targetEntity = RelationshipUtils.getTargetedEntity(player, range, LivingEntity.class);
		if (targetEntity == null) {
			targetLocation = RelationshipUtils.getTargetedLocation(source.getPlayer(), range);
		} else {
			targetLocation = targetEntity.getLocation();
		}
		world = targetLocation.getWorld();
		plugin = source.getPlugin();

		Location loc = targetLocation.clone();
		loc.add(0, maxHeight, 0);
		Vector line = MathUtils.randomOffset(down, 30);
		line.normalize();
		line.multiply(dParticle);
		int distance = MathUtils.random.nextInt(7) + 2;
		Packet<?> particle;
		int j = 0;
		Location toLoc = targetLocation.clone();
		while (true) {
			if (j > distance) {
				j = 0;
				distance = MathUtils.random.nextInt(5) + 2;
				toLoc.setY(loc.getY() - dParticle * 2);
				line = MathUtils.getDistanceVector(loc, toLoc);
				line = MathUtils.randomOffset(line, 20);
				line.normalize();
				line.multiply(dParticle);
			}
			particle = CraftMethods.getParticlePacket(EnumParticle.CRIT, true, loc, 0.1F, 0.1F, 0.1F, 0, 3);
			particlePackets.add(particle);
			particleLocations.add(loc);
			loc.add(line);
			if (loc.getY() <= targetLocation.getY()) {
				break;
			}
			j++;
		}

		particlesPerTick = particlePackets.size() / chargeTime;

	}

	@Override
	protected void progress() {
		if (i < particlePackets.size() - 1) {
			int j = i + particlesPerTick;
			if (j >= particlePackets.size()) {
				j = particlePackets.size() - 1;
			}
			CraftMethods.sendPacket(plugin, world, particlePackets.subList(i, j));
			i = j;
		}

		if (tick == chargeTime) {
			world.playSound(targetLocation, Sound.ENTITY_CREEPER_PRIMED, 1.5F, 1F);
		}

		if (tick == pauseTime) {
			lightning = world.strikeLightning(targetLocation);
			POMListener.addLightningStrike(lightning, player);
		}

		if (tick > pauseTime + 10) {
			POMListener.removeLightningStrike(lightning);
			kill();
		}
	}

}
