package main.java.org.valor.pom.schools.fire;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Line;
import main.java.org.valor.pom.schools.Progression;
import main.java.org.valor.pom.schools.Spiral;
import net.minecraft.server.v1_11_R1.EnumParticle;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Inferno extends Progression {

	private static final double range = 20;
	private static final double radius = 3;
	private static final double radius2 = radius * radius;
	private static final int numberOfLines = (int) (Math.PI * radius2);

	private List<Spiral> spirals = new ArrayList<>();
	private List<Line> lines = new ArrayList<>();
	private Location origin;
	private Player player;
	private ParagonsOfMagic plugin;

	public Inferno(POMPlayer source) {
		this.origin = RelationshipUtils.getTargetedLocation(source.getPlayer(), range);
		this.player = source.getPlayer();
		this.plugin = source.getPlugin();

		Spiral spiral;
		for (double angle = 0; angle < 360; angle += 120) {
			spiral = new Spiral(origin, 3, -.1, angle, 5, .2);
			spirals.add(spiral);
		}

		lines.add(new Line(origin, new Vector(0, 1, 0), .25, 10));
		this.isAlive = true;
	}

	@Override
	protected void progress() {
		boolean isGoing = false;

		Location spiralLocation;
		for (Spiral spiral : spirals) {
			spiral.progress();
			spiralLocation = spiral.getLocation();
			CraftMethods.sendPacket(plugin, spiralLocation.getWorld(),
					CraftMethods.getParticlePacket(EnumParticle.FLAME, true, spiralLocation, 0.2F, 0.2F, 0.2F, 0, 5));
		}

		Location lineLocation;
		for (Line line : lines) {
			line.progress();
			lineLocation = line.getLocation();
			CraftMethods.sendPacket(plugin, lineLocation.getWorld(),
					CraftMethods.getParticlePacket(EnumParticle.FLAME, true, lineLocation, 0.2F, 0.2F, 0.2F, 0, 5));

			if (!isGoing && !line.isDone()) {
				isGoing = true;
			}
		}

		if (!isGoing) {
			kill();
		}

	}

}
