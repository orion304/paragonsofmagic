package main.java.org.valor.pom.schools.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.MainThread;
import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Spell;
import main.java.org.valor.pom.schools.SpellProjectile;
import net.minecraft.server.v1_11_R1.EnumParticle;
import net.minecraft.server.v1_11_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class Gust extends SpellProjectile {

	private static final int streams = 3;
	static final double maxY = .6;
	private static final long lifeTicks = 200;
	private static final double maxWidth = 2.7;
	private static final double startWidth = .1;
	private static final double dwidth = (maxWidth - startWidth) / lifeTicks;

	private double angle = 0;
	private double width = startWidth;
	private int i = 0;

	public Gust(final LivingEntity source, final Location location, final Vector velocity) {
		super(Spell.GUST, location, velocity.clone().normalize().multiply(3D), source, false, true, false, .1);
	}

	public Gust(final POMPlayer source) {
		this(source.getPlayer(), source.getPlayer().getEyeLocation(),
				source.getPlayer().getEyeLocation().getDirection());
	}

	@Override
	public void animate() {
		// for (AirShield shield : Progression.getByClass(AirShield.class)) {
		// if (shield.isInShield(this.location)) {
		// kill();
		// return;
		// }
		// }
		this.i++;
		this.angle += 15;
		this.width += dwidth;

		this.hitbox.setHeight(this.width);
		this.hitbox.setWidth(this.width);

		if (this.location.getBlock().isLiquid()) {
			this.isAlive = false;
			return;
		}

		// if (MovingRock.isBlocked(this.hitbox)) {
		// this.isAlive = false;
		// return;
		// }

		for (SpellProjectile projectile : SpellProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!ParagonsOfMagic.isBendable(this.shooter, projectile.getLocation(), Spell.GUST)) {
				continue;
			}
			// if (projectile instanceof WaterBlast || projectile instanceof
			// EarthBlast) {
			// this.isAlive = false;
			// return;
			// } else if (projectile instanceof FireBlast) {
			// Vector v = projectile.getVelocity();
			// double d = v.length();
			// v = v.clone().add(this.velocity);
			// this.isAlive = false;
			// if (v.lengthSquared() == 0) {
			// projectile.setAlive(false);
			// return;
			// } else {
			// v.normalize().multiply(d);
			// }
			// projectile.setVelocity(v);
			// return;
			// } else if (projectile instanceof AirSuction || projectile
			// instanceof AirBlast && !this.equals(projectile)) {
			// projectile.setAlive(false);
			// this.isAlive = false;
			// return;
			// }
		}

		for (final Entity entity : this.nearbyEntities) {
			if (entity.equals(getShooter())) {
				continue;
			}

			if (!ParagonsOfMagic.isBendable(this.shooter, entity.getLocation(), this.spell)) {
				continue;
			}
			Hitbox box;
			if (entity instanceof LivingEntity) {
				box = new Hitbox((LivingEntity) entity);
			} else {
				box = new Hitbox(.5, .5);
				box.setBottomLocation(entity.getLocation());
			}

			if (this.hitbox.isInside(box)) {
				// final Vector v = this.velocity.clone().normalize();
				// final Vector vel = entity.getVelocity();
				// double velY = vel.getY();
				// double vY = v.getY();
				// if (velY > maxY) {
				// vY = 0;
				// } else if (velY + vY > maxY) {
				// vY = maxY - velY;
				// }
				// v.setY(vY / 3D);
				// final double d = v.clone().normalize().dot(vel);
				// vel.subtract(v.clone().multiply(d));
				// vel.add(v.multiply(3D));

				Vector vel = entity.getVelocity();
				Vector v = this.velocity.clone();
				if (v.getY() > maxY) {
					v.setY(maxY);
				}
				MathUtils.setComponent(vel, v);

				entity.setVelocity(vel);

				entity.setFireTicks(0);
				entity.setFallDistance(0);
				MainThread.causeFlight(this.shooter, entity);
			}
		}

		for (Block block : this.nearbyBlocks) {
			if (!this.hitbox.isInside(block)
					|| !ParagonsOfMagic.isBendable(this.shooter, block.getLocation(), this.spell)) {
				continue;
			}
			if (block.getType() == Material.FIRE) {
				// ParagonsOfMagicTools.clearBlock(block);
				block.getWorld().playSound(block.getLocation().add(.5, .5, .5), Sound.BLOCK_FIRE_EXTINGUISH, .5F, 1F);
			} else if (EnvironmentUtils.isLava(block)) {
				block.setType(block.getData() == 0x0 ? Material.OBSIDIAN : Material.COBBLESTONE);
				block.getWorld().playSound(block.getLocation().add(.5, .5, .5), Sound.BLOCK_FIRE_EXTINGUISH, .5F, 1F);
			}
		}

		Vector rotator = MathUtils.getOrthogonalVector(this.velocity, 0, this.width);
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < streams; i++) {
			Vector v = MathUtils.rotateVectorAroundVector(this.velocity, rotator, this.angle + i * 360 / streams);
			Location loc = this.location.clone().add(v);
			packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0F, 0F, 0F, 0, 1));
		}
		// packets.add(CraftMethods.getParticlePacket("crit", this.location, 0,
		// 0,
		// 0, 0, 1));
		CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);
		if (this.i % 3 == 0) {
			this.location.getWorld().playSound(this.location, Sound.ENTITY_HORSE_BREATHE, 1F,
					MathUtils.random.nextFloat() * .5F + 1F);
		}
	}
}
