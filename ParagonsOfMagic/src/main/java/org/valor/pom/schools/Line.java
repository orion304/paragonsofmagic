package main.java.org.valor.pom.schools;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class Line {

	private Location location;
	private Vector direction;
	private double dy, distance, maxdistance;

	public Line(Location origin, Vector direction, double dy, double maxdistance) {
		location = origin.clone();
		this.direction = direction.clone();
		direction.normalize().multiply(dy);
		this.dy = dy;
		this.maxdistance = maxdistance;
		distance = 0;
	}

	public Location getLocation() {
		return this.location;
	}

	public boolean isDone() {
		return distance >= maxdistance;
	}

	public void progress() {
		if (!isDone()) {
			location.add(direction);
			distance += dy;
		}
	}

}
