package main.java.org.valor.pom.schools.space;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Progression;
import net.minecraft.server.v1_11_R1.EnumParticle;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Teleport extends Progression {

	private static final double maxDistance = 50;
	private static final long teleportTime = 12L;
	private static final int numberOfStreams = 25;
	private static final double maxR = 1.8;
	private static final int ticksPerAnimation = 2;
	private static final double dr = ticksPerAnimation * maxR / teleportTime;

	private final double[] costhetas = new double[numberOfStreams];
	private final double[] sinthetas = new double[numberOfStreams];
	private final double[] cosphis = new double[numberOfStreams];
	private final double[] sinphis = new double[numberOfStreams];

	private final POMPlayer source;
	private final ParagonsOfMagic plugin;
	private double r = maxR;

	public Teleport(POMPlayer source) {
		super();
		this.source = source;
		plugin = source.getPlugin();
		Player player = source.getPlayer();
		Location location = player.getLocation();
		this.isAlive = true;

		double phi, theta;
		for (int i = 0; i < numberOfStreams; i++) {
			phi = 2 * Math.PI * MathUtils.random.nextDouble();
			theta = Math.acos(2 * MathUtils.random.nextDouble() - 1);

			costhetas[i] = Math.cos(theta);
			sinthetas[i] = Math.sin(theta);
			cosphis[i] = Math.cos(phi);
			sinphis[i] = Math.sin(phi);
		}

		source.getPlayer().getWorld().playSound(location, Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1F, 1.6F);
		// CraftMethods.sendPacket(plugin, source.getPlayer().getWorld(), new
		// PacketPlayOutNamedSoundEffect(
		// "mob.zombie.unfect", location.getX(), location.getY(),
		// location.getZ(), 1F, 1.6F));
	}

	private void animate() {
		if (tick % ticksPerAnimation != 0) {
			return;
		}
		Location location = source.getPlayer().getLocation();
		World world = location.getWorld();
		double x, y, z, dx, dy, dz;
		x = location.getX();
		y = location.getY() + 1.2;
		z = location.getZ();
		Location loc;
		for (int i = 0; i < numberOfStreams; i++) {
			dx = r * cosphis[i] * sinthetas[i];
			dy = r * costhetas[i];
			dz = r * sinphis[i] * sinthetas[i];
			loc = new Location(world, x + dx, y + dy, z + dz);
			CraftMethods.sendPacket(plugin, world,
					CraftMethods.getParticlePacket(EnumParticle.CRIT_MAGIC, true, loc, 0F, 0F, 0F, 0, 1));
		}

		r -= dr;
	}

	@Override
	protected void progress() {
		Player player = this.source.getPlayer();
		if (player.isDead() || !player.isOnline()) {
			kill();
			return;
		}

		if (this.tick >= teleportTime) {
			teleport();
		} else {
			animate();
		}
	}

	private void teleport() {
		Player player = this.source.getPlayer();
		Location playerLocation = player.getLocation();
		float pitch = playerLocation.getPitch();
		float yaw = playerLocation.getYaw();
		Location location = RelationshipUtils.getTargetedLocation(player, maxDistance);
		location.setPitch(pitch);
		location.setYaw(yaw);
		location.setX(location.getBlockX());
		location.setY(location.getBlockY());
		location.setZ(location.getBlockZ());
		player.teleport(location);
		player.setFallDistance(0);
		location.add(0, 1.2, 0);
		CraftMethods.sendPacket(plugin, player.getWorld(),
				CraftMethods.getParticlePacket(EnumParticle.SMOKE_LARGE, true, location, .4F, .4F, .4F, 0, 10));
		kill();
	}

}
