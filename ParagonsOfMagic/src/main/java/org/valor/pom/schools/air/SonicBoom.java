package main.java.org.valor.pom.schools.air;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.MainThread;
import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Progression;
import main.java.org.valor.pom.schools.Spell;
import main.java.org.valor.pom.schools.SpellProjectile;
import net.minecraft.server.v1_11_R1.EnumParticle;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class SonicBoom extends Progression {

	private static final Vector X = new Vector(1, 0, 0);
	private static final Vector Y = new Vector(0, 1, 0);
	private static final Vector Z = new Vector(0, 0, 1);
	private static final double maxRadius = 15;
	private static final double minRadius = .5;
	private static final long expandTime = 20L;
	private static final double dR = (maxRadius - minRadius) / expandTime;
	private static final double dTheta = 15;

	private static final double dT = dTheta * 4;
	private static final int streams = 15;
	private static final double dStream = 180D / (streams + 1);
	private final double Xangle;
	private final double Yangle;
	private final double Zangle;

	private final POMPlayer source;
	private double angle = 0;
	private double radius = minRadius;
	private double radius2 = this.radius * this.radius;

	private Vector axis = MathUtils.randomOffset(Y, 180);

	public final Hitbox hitbox = new Hitbox(maxRadius + 2, maxRadius + 2);

	public SonicBoom(POMPlayer player) {
		super();

		this.source = player;
		this.hitbox.setCenterLocation(this.source.getPlayer().getLocation());
		this.Xangle = MathUtils.random.nextDouble() * 1.25 + .25;
		this.Yangle = MathUtils.random.nextDouble() * 1.25 + .25;
		this.Zangle = MathUtils.random.nextDouble() * 1.25 + .25;

		this.isAlive = true;
	}

	public boolean isInShield(Location location) {
		if (!location.getWorld().equals(this.source.getPlayer().getWorld())) {
			return false;
		}
		return location.distanceSquared(this.source.getPlayer().getLocation()) <= this.radius2;
	}

	@Override
	protected void progress() {
		if (tick > expandTime) {
			kill();
			return;
		}
		if (this.source.getPlayer().isDead() || !this.source.getPlayer().isOnline()) {
			kill();
			return;
		}
		this.hitbox.setCenterLocation(this.source.getPlayer().getLocation());
		double theta = this.angle;
		Location playerLoc = this.source.getPlayer().getLocation();
		Location loc;
		World world = playerLoc.getWorld();
		for (int i = 1; i <= streams; i++) {
			Vector v = MathUtils.getOrthogonalVector(this.axis, theta, this.radius);
			Vector v1 = MathUtils.rotateVectorAroundVector(v, this.axis, i * dStream);

			loc = playerLoc.clone().add(v1.multiply(this.radius));

			if (ParagonsOfMagic.isBendable(this.source.getPlayer(), loc, Spell.SONIC_BOOM)) {
				CraftMethods.sendPacket(ParagonsOfMagic.getPlugin(), world,
						CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0, 0, 0, 0, 1));
				world.playSound(loc, Sound.ENTITY_HORSE_BREATHE, .5F, .4F);
			}

			theta = theta + dT;
		}

		for (Entity entity : EnvironmentUtils.getEntitiesAroundPoints(playerLoc, this.radius)) {
			Location eLoc = entity.getLocation();
			if (entity != this.source.getPlayer() && !eLoc.getBlock().isLiquid()
					&& ParagonsOfMagic.isBendable(this.source.getPlayer(), entity.getLocation(), Spell.SONIC_BOOM)
					&& eLoc.distanceSquared(playerLoc) > 1) {
				Vector v = MathUtils.getDistanceVector(playerLoc, eLoc);
				v.normalize().multiply(.9);
				double dx = eLoc.getX() - playerLoc.getX();
				double dz = eLoc.getZ() - playerLoc.getZ();
				entity.setVelocity(v.add(new Vector(-dz, 0, dx).normalize().multiply(1)));
				MainThread.causeFlight(this.source.getPlayer(), entity);
			}
		}

		for (SpellProjectile projectile : SpellProjectile.getProjectilesInHitbox(this.hitbox)) {
			// if (!ParagonsOfMagic.isBendable(this.source.getPlayer(),
			// projectile.getLocation(), Spell.AIR_SHIELD)) {
			// continue;
			// }
			// if ((projectile instanceof AirBlast
			// || projectile instanceof AirSuction
			// || projectile instanceof FireBlast || projectile instanceof
			// AirPunch)
			// && projectile.getLocation().distanceSquared(playerLoc) <=
			// this.radius
			// * this.radius) {
			// projectile.setAlive(false);
			// }
		}

		this.angle += dTheta;
		if (this.radius < maxRadius) {
			this.radius += dR;
			if (this.radius > maxRadius) {
				this.radius = maxRadius;
			}
			this.radius2 = this.radius * this.radius;
		}

		// this.axis = MathUtils.rotateVectorAroundVector(X, this.axis,
		// this.Xangle);
		// this.axis = MathUtils.rotateVectorAroundVector(Y, this.axis,
		// this.Yangle);
		// this.axis = MathUtils.rotateVectorAroundVector(Z, this.axis,
		// this.Zangle);

	}

}
