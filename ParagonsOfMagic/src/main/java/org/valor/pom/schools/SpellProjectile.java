package main.java.org.valor.pom.schools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.ParagonsOfMagic;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.projectile.CustomProjectile;

public abstract class SpellProjectile extends CustomProjectile {

	public static final List<SpellProjectile> projectiles = new ArrayList<>();

	public static Set<SpellProjectile> getProjectilesInHitbox(Hitbox hitbox) {
		Set<SpellProjectile> hits = new HashSet<>();
		for (SpellProjectile projectile : projectiles) {
			if (projectile.hitbox.isInside(hitbox)) {
				hits.add(projectile);
			}
		}
		return hits;
	}

	protected double rangeSquared = 2500;
	protected final Location source;

	protected final Spell spell;

	public SpellProjectile(Spell spell, final Location location, final Vector velocity, final LivingEntity shooter,
			final boolean hasGravity, final boolean hitsBlocks, final boolean hitsEntities, final double hitRadius) {
		super(ParagonsOfMagic.getPlugin(), location, velocity, shooter, hasGravity, hitsBlocks, hitsEntities,
				hitRadius);
		this.source = location.clone();
		this.spell = spell;
		projectiles.add(this);
	}

	@Override
	public Vector getVelocity() {
		return this.velocity;
	}

	public void kill() {
		this.isAlive = false;
	}

	@Override
	public void run() {
		if (this.isAlive) {
			if (this.location.distanceSquared(this.source) > this.rangeSquared
					|| !ParagonsOfMagic.isCastable(this.shooter, this.location, this.spell)) {
				this.isAlive = false;
			}
		}
		super.run();
		if (!this.isAlive) {
			projectiles.remove(this);
		}
	}

	public void setAlive(boolean alive) {
		this.isAlive = alive;
	}

	public void setShooter(LivingEntity shooter) {
		this.shooter = shooter;
	}

	public void setVelocity(Vector vector) {
		this.velocity = vector;
	}

}
