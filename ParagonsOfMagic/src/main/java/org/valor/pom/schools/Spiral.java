package main.java.org.valor.pom.schools;

import org.bukkit.Location;

public class Spiral {

	private Location location;
	private double radius, dr, angle, dangle, dy;
	private double ox, oy, oz;
	private double x, y, z;

	public Spiral(Location origin, double radius, double dr, double angle, double dangle, double dy) {
		location = origin.clone();
		this.radius = radius;
		this.dr = dr;
		this.angle = Math.toRadians(angle);
		this.dangle = Math.toRadians(dangle);
		this.dy = dy;

		ox = origin.getX();
		oy = origin.getY();
		oz = origin.getZ();

		x = ox;
		y = oy;
		z = oz;
	}

	public Location getLocation() {
		return this.location;
	}

	public void progress() {
		y += dy;
		radius += dr;
		angle += dangle;
		x = ox + radius * Math.cos(angle);
		z = oz + radius * Math.sin(angle);
		location.setX(x);
		location.setY(y);
		location.setZ(z);
	}

}
