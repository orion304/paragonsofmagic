package main.java.org.valor.pom.schools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;

import src.main.java.org.orion304.menu.MenuItem;

public enum Spell {

	TELEPORT(School.SPACE, "Teleports you to your targeted location"), GUST(School.AIR,
			"A gust of wind that pushes anything in its path away"), SONIC_BOOM(School.AIR,
					"Creates a rush of air outwards from the user, blowing everything in its path away"), AIR_SUCTION(
							School.AIR, "Description"), TORNADO(School.AIR, "Description"), LIGHTNING(School.AIR,
									"Strikes a bolt of lightning at your targeted location"), INFERNO(School.FIRE,
											"Creates an inferno at your targeted location.");

	private static Map<School, List<Spell>> spells = new HashMap<>();

	static {

		for (School school : School.values()) {
			spells.put(school, new ArrayList<Spell>());
		}

		for (Spell spell : values()) {
			spells.get(spell.school).add(spell);
		}

	}

	public static List<Spell> getSpells(School school) {
		return spells.get(school);
	}

	private final String permission, name, description, title;
	private final School school;
	private final MenuItem menuItem;

	private Spell(School school, String description) {
		this.school = school;
		this.description = description;
		StringBuilder builder = new StringBuilder();
		String[] bigSplit = name().split("__");
		for (int i = 0; i < bigSplit.length; i++) {
			String[] smallSplit = bigSplit[i].split("_");
			for (int j = 0; j < smallSplit.length; j++) {
				String string = smallSplit[j];
				char c = string.charAt(0);
				String rest = string.substring(1);
				builder.append(c);
				builder.append(rest.toLowerCase());
				if (j != smallSplit.length - 1) {
					builder.append(" ");
				}
			}
			if (i != bigSplit.length - 1) {
				builder.append(" / ");
			}
		}
		this.name = builder.toString();
		this.permission = "pom." + school.name().toLowerCase().replaceAll("_", "") + "."
				+ name().toLowerCase().replaceAll("_", "");
		this.title = school.getColor() + name;
		this.menuItem = new MenuItem(title, school.getMaterial(), 1, ChatColor.WHITE + description);
	}

	public String description() {
		return this.description;
	}

	public MenuItem getMenuItem() {
		return this.menuItem;
	}

	public String getPermissionNode() {
		return this.permission;
	}

	public School getSchool() {
		return this.school;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
