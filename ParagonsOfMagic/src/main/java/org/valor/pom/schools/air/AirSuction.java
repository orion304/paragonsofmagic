package main.java.org.valor.pom.schools.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Progression;
import main.java.org.valor.pom.schools.Spell;
import main.java.org.valor.pom.schools.SpellProjectile;
import net.minecraft.server.v1_11_R1.EnumParticle;
import net.minecraft.server.v1_11_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;

public class AirSuction extends SpellProjectile {

	private static final int streams = 5;
	private static final double dtheta = 360D / 5;

	private double angle = 0;
	private double width = 1.5;
	private int i = 0;
	private final boolean isSpouting;

	public AirSuction(final LivingEntity source, final Location location, final Vector velocity, boolean isSpouting) {
		super(Spell.AIR_SUCTION, location, velocity.clone().normalize().multiply(3D), source, false, true, false, .1);
		this.rangeSquared = 400;
		this.isSpouting = isSpouting;
	}

	@Override
	public void animate() {
		for (SonicBoom shield : Progression.getByClass(SonicBoom.class)) {
			if (shield.isInShield(this.location)) {
				kill();
				return;
			}
		}
		this.angle += 5;
		this.i++;
		if (this.width > .4) {
			this.width -= .02;
		}
		this.hitbox.setHeight(this.width);
		this.hitbox.setWidth(this.width);
		if (this.location.getBlock().isLiquid()) {
			this.isAlive = false;
			return;
		}

		// if (MovingRock.isBlocked(this.hitbox)) {
		// this.isAlive = false;
		// return;
		// }

		for (SpellProjectile projectile : SpellProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (!ParagonsOfMagic.isBendable(this.shooter, projectile.getLocation(), this.spell)) {
				continue;
			}
			// if (projectile instanceof WaterBlast || projectile instanceof
			// EarthBlast) {
			// this.isAlive = false;
			// return;
			// } else if (projectile instanceof FireBlast) {
			// Vector v = projectile.getVelocity();
			// double d = v.length();
			// v = v.clone().add(this.velocity);
			// this.isAlive = false;
			// if (v.lengthSquared() == 0) {
			// projectile.setAlive(false);
			// return;
			// } else {
			// v.normalize().multiply(d);
			// }
			// projectile.setVelocity(v);
			// return;
			// } else if (projectile instanceof AirBlast || projectile
			// instanceof AirSuction && !this.equals(projectile)) {
			// projectile.setAlive(false);
			// this.isAlive = false;
			// return;
			// }
		}

		for (final Entity entity : this.nearbyEntities) {
			if (entity.equals(getShooter())
					|| !ParagonsOfMagic.isBendable(this.shooter, entity.getLocation(), this.spell)) {
				continue;
			}
			Hitbox box;
			if (entity instanceof LivingEntity) {
				box = new Hitbox((LivingEntity) entity);
			} else {
				box = new Hitbox(.5, .5);
				box.setBottomLocation(entity.getLocation());
			}

			if (this.hitbox.isInside(box)) {
				// final Vector v = this.velocity.clone().normalize();
				// final Vector vel = entity.getVelocity();
				// double velY = vel.getY();
				// double vY = v.getY();
				// if (velY > maxY) {
				// vY = 0;
				// } else if (velY + vY > maxY) {
				// vY = maxY - velY;
				// }
				// v.setY(vY / 3D);
				// final double d = v.clone().normalize().dot(vel);
				// vel.subtract(v.clone().multiply(d));
				// vel.add(v.multiply(3D));

				Vector vel = entity.getVelocity();
				Vector v = this.velocity.clone();
				if (this.isSpouting) {
					v.setY(0);
				} else if (v.getY() > Gust.maxY) {
					v.setY(Gust.maxY);
				}
				MathUtils.setComponent(vel, v);

				entity.setVelocity(vel);
				entity.setFireTicks(0);
				entity.setFallDistance(0);
				// MainThread.causeFlight(this.shooter, entity);
			}
		}

		for (Block block : this.nearbyBlocks) {
			if (!this.hitbox.isInside(block)
					|| !ParagonsOfMagic.isBendable(this.shooter, block.getLocation(), this.spell)) {
				continue;
			}
			if (block.getType() == Material.FIRE) {
				// BendingTools.clearBlock(block);
				block.getWorld().playSound(block.getLocation().add(.5, .5, .5), Sound.BLOCK_FIRE_EXTINGUISH, .5F, 1F);
			} else if (EnvironmentUtils.isLava(block)) {
				block.setType(block.getData() == 0x0 ? Material.OBSIDIAN : Material.COBBLESTONE);
				block.getWorld().playSound(block.getLocation().add(.5, .5, .5), Sound.BLOCK_FIRE_EXTINGUISH, .5F, 1F);
			}
		}

		List<Packet<?>> packets = new ArrayList<>();
		Vector rotator = MathUtils.getOrthogonalVector(this.velocity, this.angle, this.width);
		for (int i = 0; i < streams; i++) {
			Vector v = MathUtils.rotateVectorAroundVector(this.velocity, rotator, dtheta * i);
			Location loc = this.location.clone().add(v.multiply(MathUtils.random.nextDouble() * .6 + .7));
			packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0F, 0F, 0F, 0, 1));
		}
		CraftMethods.sendPacket(this.plugin, this.location.getWorld(), packets);
		if (this.i % 3 == 0) {
			this.location.getWorld().playSound(this.location, Sound.ENTITY_HORSE_BREATHE, 1F,
					MathUtils.random.nextFloat() * .5F + 1F);
		}

	}

}
