package main.java.org.valor.pom.schools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Progression {

	private static final List<Progression> instances = new ArrayList<>();
	private static final Map<Class<? extends Progression>, List<? super Progression>> instancesByClass = new HashMap<>();

	public static <T extends Progression> List<T> getByClass(Class<T> c) {
		if (instancesByClass.containsKey(c)) {
			return (List<T>) instancesByClass.get(c);
		} else {
			return new ArrayList<T>();
		}
	}

	public static void handle() {
		List<Progression> toRemove = new ArrayList<>();
		for (Progression instance : instances) {
			if (instance.isAlive) {
				instance.progress();
				instance.tick++;
			}
			if (!instance.isAlive) {
				toRemove.add(instance);
			}
		}
		for (Progression instance : toRemove) {
			instancesByClass.get(instance.getClass()).remove(instance);
			instances.remove(instance);
		}
	}

	public static void killAll() {
		for (Progression instance : instances) {
			instance.kill();
		}
		instances.clear();
	}

	protected long tick;

	public boolean isAlive;

	public Progression() {
		instances.add(this);
		Class<? extends Progression> c = getClass();
		if (!instancesByClass.containsKey(c)) {
			instancesByClass.put(c, new ArrayList<Progression>());
		}
		instancesByClass.get(c).add(this);
	}

	public void kill() {
		this.isAlive = false;
	}

	protected abstract void progress();

}
