package main.java.org.valor.pom.schools.air;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.util.Vector;

import main.java.org.valor.pom.main.MainThread;
import main.java.org.valor.pom.main.POMPlayer;
import main.java.org.valor.pom.main.ParagonsOfMagic;
import main.java.org.valor.pom.schools.Progression;
import main.java.org.valor.pom.schools.Spell;
import main.java.org.valor.pom.schools.SpellProjectile;
import net.minecraft.server.v1_11_R1.EnumParticle;
import net.minecraft.server.v1_11_R1.Packet;
import src.main.java.org.orion304.Hitbox;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.EnvironmentUtils;
import src.main.java.org.orion304.utils.MathUtils;
import src.main.java.org.orion304.utils.RelationshipUtils;

public class Tornado extends Progression {

	private static final int streams = 15;
	private static final double maxHeight = 23;
	private static final double startHeight = 3;
	private static final double decreaseHeight = .2;
	private static final double tan = Math.tan(Math.toRadians(15));
	private static final double dTheta = Math.toRadians(30);

	private static final long channelTime = 10L;
	private static final long decayTime = channelTime + 40L;
	private static final double dHeight = (maxHeight - startHeight) / channelTime;

	private static final double speed = .05;

	private final POMPlayer source;

	private Location location;
	private Vector velocity = null;
	private double height = startHeight;
	private final List<Double> radii = new ArrayList<>();
	private double theta = 0;
	private final Hitbox hitbox = new Hitbox(maxHeight, maxHeight);

	public Tornado(POMPlayer source) {
		this.source = source;
		Player player = source.getPlayer();
		for (int i = 0; i < streams; i++) {
			this.radii.add(MathUtils.random.nextDouble() * .4 + .8);
		}
		this.isAlive = true;

		this.location = RelationshipUtils.getTargetedLocation(player, .5);
		this.location.setY(player.getLocation().getY() - 2);

		direct();
	}

	public void direct() {
		Vector vector = this.source.getPlayer().getEyeLocation().getDirection();
		vector.setY(0);
		vector.normalize().multiply(1.2);
		this.velocity = vector;
		if (this.location == null) {
			kill();
		}
	}

	public boolean isDirected() {
		return this.velocity != null;
	}

	@Override
	protected void progress() {
		this.location.getWorld().playSound(this.location, Sound.ENTITY_HORSE_BREATHE, 1F,
				MathUtils.random.nextFloat() * .5F + 1F);
		this.location.add(this.velocity);
		Location loc;
		World world = this.location.getWorld();
		double y = this.location.getY();
		double dy = this.height / streams;
		this.hitbox.setBottomLocation(this.location.clone().add(0, 3 * dy, 0));
		y = y - dy;
		double h = 3 * dy;
		double t = this.theta;
		double x, z, r;
		double xi = this.location.getX();
		double zi = this.location.getZ();
		List<Packet<?>> packets = new ArrayList<>();
		for (int i = 0; i < streams; i++) {
			r = h * tan * this.radii.get(i);
			x = r * Math.cos(t) + xi;
			z = r * Math.sin(t) + zi;
			loc = new Location(world, x, y, z);
			if (ParagonsOfMagic.isBendable(this.source.getPlayer(), loc, Spell.TORNADO)) {
				packets.add(CraftMethods.getParticlePacket(EnumParticle.SMOKE_NORMAL, true, loc, 0, 0, 0, 0, 1));
			}

			t += dTheta * 3;
			y += dy;
			h += dy;
		}
		CraftMethods.sendPacket(ParagonsOfMagic.getPlugin(), world, packets);

		Vector v;
		for (Entity entity : EnvironmentUtils.getEntitiesAroundPoints(this.location, this.height)) {
			if (!ParagonsOfMagic.isBendable(this.source.getPlayer(), entity.getLocation(), Spell.TORNADO)) {
				continue;
			}
			v = throwAt(entity.getLocation(), dy, !(entity instanceof Player));
			if (v != null && !entity.equals(this.source.getPlayer())) {
				entity.setVelocity(v);
				if (entity instanceof Projectile) {
					((Projectile) entity).setShooter(this.source.getPlayer());
				}
				entity.setFallDistance(0);
				MainThread.causeFlight(this.source.getPlayer(), entity);
			}
		}

		for (SpellProjectile projectile : SpellProjectile.getProjectilesInHitbox(this.hitbox)) {
			if (projectile.getShooter() == this.source.getPlayer()
					|| !ParagonsOfMagic.isBendable(this.source.getPlayer(), projectile.getLocation(), Spell.TORNADO)) {
				continue;
			}
			v = throwAt(projectile.getLocation(), dy, true);
			if (v != null) {
				if (projectile instanceof Gust || projectile instanceof AirSuction) {
					projectile.kill();
				}
				// if (projectile instanceof FireBlast) {
				// projectile.setVelocity(v);
				// projectile.setShooter(this.source.getPlayer());
				// }
			}
		}

		if (tick < channelTime) {
			if (this.height < maxHeight) {
				this.height += dHeight;
				if (this.height > maxHeight) {
					this.height = maxHeight;
				}
			}
		} else if (tick > decayTime) {
			this.height -= decreaseHeight;
			if (this.height <= 0) {
				kill();
			}
		}

		this.theta += dTheta;
	}

	private Vector throwAt(Location location, double h, boolean up) {
		Vector distance = MathUtils.getDistanceVector(location, this.location);
		distance.setY(0);

		double y = location.getY();
		double yo = this.location.getY() - 3 * h;

		double dy = y - yo;

		if (dy < 0) {
			return null;
		}

		double r2 = distance.lengthSquared();
		double R = dy * tan;

		if (R * R < r2) {
			return null;
		}

		Vector v = new Vector(distance.getZ(), 0, -distance.getX());
		v.normalize().multiply(speed);
		v.subtract(distance.normalize().multiply(13 * speed));
		if (this.velocity != null) {
			v.add(this.velocity);
		}
		// if (dy < 7) {
		v.setY(1.3);
		// }
		return v;

	}

}
