package main.java.org.valor.pom.schools;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import src.main.java.org.orion304.menu.MenuItem;

public enum School {

	FIRE(ChatColor.RED, Material.BLAZE_POWDER, "Description"), ICE(ChatColor.AQUA, Material.PACKED_ICE,
			"Description"), NATURE(ChatColor.DARK_GREEN, Material.SAPLING, "Description"), EARTH(ChatColor.GREEN,
					Material.DIRT, "Description"), TIME(ChatColor.YELLOW, Material.WATCH, "Description"), AIR(
							ChatColor.GRAY, Material.FEATHER,
							"Description"), FORCE(ChatColor.DARK_AQUA, Material.NETHER_STAR, "Description"), DIVINE(
									ChatColor.GOLD, Material.BLAZE_ROD, "Description"), ILLUSION(ChatColor.BLUE,
											Material.SUGAR, "Description"), NECROMANCY(ChatColor.DARK_GRAY,
													Material.EYE_OF_ENDER, "Description"), SPACE(ChatColor.DARK_PURPLE,
															Material.PRISMARINE_CRYSTALS, "Description");

	private final String name, description, title;
	private final ChatColor color;
	private final Material material;
	private final MenuItem menuItem;

	private School(ChatColor color, Material material, String description) {
		this.color = color;
		this.material = material;
		this.description = description;
		StringBuilder builder = new StringBuilder();
		String[] bigSplit = name().split("__");
		for (int i = 0; i < bigSplit.length; i++) {
			String[] smallSplit = bigSplit[i].split("_");
			for (int j = 0; j < smallSplit.length; j++) {
				String string = smallSplit[j];
				char c = string.charAt(0);
				String rest = string.substring(1);
				builder.append(c);
				builder.append(rest.toLowerCase());
				if (j != smallSplit.length - 1) {
					builder.append(" ");
				}
			}
			if (i != bigSplit.length - 1) {
				builder.append(" / ");
			}
		}
		this.name = builder.toString();
		title = color + name;
		menuItem = new MenuItem(title, material, 1, ChatColor.WHITE + description);
	}

	public String description() {
		return this.description;
	}

	public ChatColor getColor() {
		return this.color;
	}

	public Material getMaterial() {
		return this.material;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
