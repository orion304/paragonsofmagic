package main.java.org.valor.pom.main;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerKickEvent;

import src.main.java.org.orion304.menu.Menu;
import src.main.java.org.orion304.menu.MenuItemClickEvent;

public class POMListener implements Listener {

	private static final Map<LightningStrike, Player> lightningStrikes = new HashMap<>();

	public static void addLightningStrike(LightningStrike strike, Player player) {
		lightningStrikes.put(strike, player);
	}

	public static void removeLightningStrike(LightningStrike strike) {
		lightningStrikes.remove(strike);
	}

	private final ParagonsOfMagic plugin;

	public POMListener(ParagonsOfMagic plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onMenuClick(MenuItemClickEvent event) {
		Menu menu = event.getMenu();
		if (menu instanceof POMSchoolMenu) {
			POMSchoolMenu schoolMenu = (POMSchoolMenu) menu;
			schoolMenu.registerClick(event.getSlot());
		} else if (menu instanceof POMSpellMenu) {
			POMSpellMenu spellMenu = (POMSpellMenu) menu;
			spellMenu.registerClick(event.getSlot());
		} else if (menu instanceof POMAssignSlotMenu) {
			POMAssignSlotMenu assignSlotMenu = (POMAssignSlotMenu) menu;
			assignSlotMenu.registerClick(event.getSlot());
		}
	}

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		Entity entity = event.getEntity();

		if (!event.isCancelled() && event.getCause() == DamageCause.FALL) {
			LivingEntity lEntity;
			if (entity instanceof LivingEntity) {
				lEntity = (LivingEntity) entity;
				LivingEntity thrower = MainThread.getThrower(lEntity);
				if (thrower != null) {
					event.setCancelled(true);
					lEntity.damage(event.getDamage(), thrower);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerDamageByEntity(EntityDamageByEntityEvent event) {
		Entity entity = event.getEntity();
		Entity damager = event.getDamager();

		if (!(entity instanceof LivingEntity)) {
			return;
		}

		LivingEntity lEntity = (LivingEntity) entity;

		if (damager instanceof LightningStrike) {
			LightningStrike strike = (LightningStrike) damager;
			if (lightningStrikes.containsKey(strike)) {
				Player player = lightningStrikes.get(strike);
				event.setCancelled(true);
				if (player != null) {
					lEntity.damage(6, player);
					lightningStrikes.replace(strike, null);
				}
			}
		}
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent event) {
		Player player = event.getPlayer();
		if (MainThread.getThrower(player) != null) {
			event.setCancelled(true);
			event.setLeaveMessage("");
			event.setReason("");
		}
	}

}
