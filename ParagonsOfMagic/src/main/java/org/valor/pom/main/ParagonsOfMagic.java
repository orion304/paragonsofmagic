package main.java.org.valor.pom.main;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import main.java.org.valor.pom.schools.Spell;
import net.minecraft.server.v1_11_R1.EnumParticle;
import src.main.java.org.orion304.OrionPlugin;
import src.main.java.org.orion304.SQLHandler;
import src.main.java.org.orion304.player.CustomPlayerHandler;
import src.main.java.org.orion304.utils.CraftMethods;
import src.main.java.org.orion304.utils.RelationshipUtils;
import src.main.java.org.orion304.utils.ServerUtils;

public class ParagonsOfMagic extends OrionPlugin {

	public static ParagonsOfMagic plugin;

	public static boolean isBendable(LivingEntity entity, Location location, Spell type) {
		Player player = null;
		if (entity instanceof Player) {
			player = (Player) entity;
		}
		return isBendable(player, location, type);
	}

	public static boolean isBendable(Player player, Location location, Spell type) {
		return true;
	}

	public static boolean isCastable(LivingEntity entity, Location location, Spell spell) {
		Player player = null;
		if (entity instanceof Player) {
			player = (Player) entity;
		}
		return isBendable(player, location, spell);
	}

	public CustomPlayerHandler<ParagonsOfMagic, POMPlayer> handler;

	public SQLHandler sqlHandler;

	@Override
	public void disable() {

	}

	@Override
	public void enable() {
		plugin = this;
		this.sqlHandler = new SQLHandler(this, "localhost", 3306, "pom", "root", "fagba11z");
		this.handler = new CustomPlayerHandler<ParagonsOfMagic, POMPlayer>(this, POMPlayer.class);

		this.manager.registerEvents(new POMListener(this), this);
		this.scheduler.runTaskTimer(this, new MainThread(this), 0L, 1L);
	}

	@Override
	public CustomPlayerHandler<ParagonsOfMagic, POMPlayer> getCustomPlayerHandler() {
		return handler;
	}

	@Override
	public boolean onCommand(final CommandSender sender, final Command cmd, final String lbl, final String[] args) {
		String label = cmd.getName();
		if (sender instanceof Player) {
			Player player = (Player) sender;
			POMPlayer pPlayer = handler.getCustomPlayer(player);
			try {
				if (label.equalsIgnoreCase("particle")) {
					if (args.length == 0) {
						String string = "";
						for (EnumParticle e : EnumParticle.values()) {
							string += e + ", ";
						}
						string = string.substring(0, string.length() - 2);
						ServerUtils.sendMessage(player, string);
					} else {
						Location location = RelationshipUtils.getTargetedLocation(player, 4);
						CraftMethods.sendPacket(this, location.getWorld(),
								CraftMethods.getParticlePacket(EnumParticle.valueOf(args[0]), true, location, 0F, 0F,
										0F, Float.parseFloat(args[1]), Integer.parseInt(args[2])));
					}
				} else if (label.equalsIgnoreCase("sound")) {
					if (args.length == 0) {
						String string = "";
						for (Sound e : Sound.values()) {
							string += e + ", ";
						}
						string = string.substring(0, string.length() - 2);
						ServerUtils.sendMessage(player, string);
					} else {
						player.getWorld().playSound(player.getLocation(), Sound.valueOf(args[0].toUpperCase()), 1F,
								Float.parseFloat(args[1]));
					}
				} else if (label.equalsIgnoreCase("csound")) {
					Location location = player.getLocation();
					// CraftMethods.sendPacket(this, player.getWorld(), new
					// PacketPlayOutNamedSoundEffect(args[0],
					// location.getX(), location.getY(), location.getZ(), 1F,
					// Float.parseFloat(args[1])));

				} else if (label.equalsIgnoreCase("test")) {

				} else if (label.equalsIgnoreCase("pom")) {
					new POMSchoolMenu(pPlayer);
				}
			} catch (Exception e) {
				sender.sendMessage("Nope");
				e.printStackTrace();
			}

		}
		return true;
	}

}
