package main.java.org.valor.pom.main;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import main.java.org.valor.pom.schools.Spell;
import src.main.java.org.orion304.menu.Menu;
import src.main.java.org.orion304.menu.MenuItem;
import src.main.java.org.orion304.utils.ServerUtils;

public class POMAssignSlotMenu extends Menu {

	private static final MenuItem cancelItem = new MenuItem(ChatColor.LIGHT_PURPLE + "Cancel assigning spell",
			Material.REDSTONE, 1);

	private final POMPlayer pPlayer;
	private final Spell spell;

	public POMAssignSlotMenu(POMPlayer source, Spell spell) {
		super(source.getPlugin(), "Assign the spell to a slot", 18);

		this.pPlayer = source;
		this.spell = spell;

		MenuItem item;
		for (int i = 0; i < 9; i++) {
			Spell spellI = source.getSpell(i);
			int slot = i + 1;
			String assignString = ChatColor.GRAY + "Click to assign " + spell.getTitle() + ChatColor.GRAY + " to slot "
					+ slot;
			if (spellI != null) {
				item = new MenuItem(ChatColor.WHITE + "Slot " + slot, spellI.getSchool().getMaterial(), 1,
						spellI.getTitle(), spellI.description(), assignString);
				addMenuItem(i, item);
			} else {
				item = new MenuItem(ChatColor.WHITE + "Slot " + slot, Material.SNOW_BALL, 1,
						ChatColor.RESET.toString() + ChatColor.ITALIC + "No spell assigned", assignString);
				addMenuItem(i, item);
			}
		}

		addMenuItem(17, cancelItem);

		openMenu(pPlayer.getPlayer());
	}

	public void registerClick(int slot) {
		pPlayer.getPlayer().closeInventory();
		if (slot < 9) {
			pPlayer.setSpell(slot, spell);
			ServerUtils.sendMessage(pPlayer.getPlayer(),
					ChatColor.GRAY + "Assigned " + spell.getTitle() + ChatColor.GRAY + " to slot " + (slot + 1) + ".");
		}
		new POMSpellMenu(pPlayer, spell.getSchool());

	}

}
