package main.java.org.valor.pom.main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import main.java.org.valor.pom.schools.Progression;
import main.java.org.valor.pom.schools.Spell;
import main.java.org.valor.pom.schools.air.Gust;
import main.java.org.valor.pom.schools.air.Lightning;
import main.java.org.valor.pom.schools.air.SonicBoom;
import main.java.org.valor.pom.schools.air.Tornado;
import main.java.org.valor.pom.schools.fire.Inferno;
import main.java.org.valor.pom.schools.space.Teleport;
import src.main.java.org.orion304.player.CustomPlayer;
import src.main.java.org.orion304.player.CustomPlayerHandler;
import src.main.java.org.orion304.utils.ServerUtils;

public class POMPlayer extends CustomPlayer<ParagonsOfMagic> {

	private static final Map<Spell, Long> spellCooldowns = new HashMap<>();

	private static final PotionEffect speed = new PotionEffect(PotionEffectType.SPEED, 4, 1);
	private static final PotionEffect jump = new PotionEffect(PotionEffectType.JUMP, 4, 2);

	static {

		spellCooldowns.put(Spell.TELEPORT, 80L);
		spellCooldowns.put(Spell.GUST, 30L);
		spellCooldowns.put(Spell.TORNADO, 90L);
		spellCooldowns.put(Spell.SONIC_BOOM, 100L);
		spellCooldowns.put(Spell.LIGHTNING, 80L);
		spellCooldowns.put(Spell.INFERNO, 100L);

	}

	private final List<Progression> progressions = new ArrayList<>();

	private final Spell[] spells = { null, null, null, null, null, null, null, null, null };
	private final Map<Spell, Long> cooldowns = new HashMap<>();
	private long globalCooldown = 0;

	public POMPlayer(UUID playerUUID, CustomPlayerHandler<ParagonsOfMagic, POMPlayer> handler) {
		super(playerUUID, handler);
	}

	@Override
	protected void asyncPreLogin(AsyncPlayerPreLoginEvent event) {
		try {
			PreparedStatement statement = this.plugin.sqlHandler.getStatement("SELECT * FROM playerData WHERE uuid=?;");
			statement.setString(1, this.playerUUID.toString());
			final ResultSet set = this.plugin.sqlHandler.get(statement);
			if (set.first()) {
				for (int i = 0; i < 9; i++) {
					final String spell = set.getString("slot" + i);
					if (spell != null) {
						try {
							this.spells[i] = Spell.valueOf(spell);
						} catch (Exception e) {
							ServerUtils.verbose(spell + " doesn't exist");
						}
					}
				}
			} else {
				statement = this.plugin.sqlHandler.getStatement("INSERT INTO playerData (uuid) values (?);");
				statement.setString(1, this.playerUUID.toString());
				this.plugin.sqlHandler.update(statement);
			}
		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	public void cooldown() {
		cooldown(null);
	}

	private void cooldown(Spell spell) {
		this.globalCooldown = this.tick + 5L;
		cooldownNoGlobal(spell);
	}

	public void cooldownNoGlobal(Spell spell) {
		if (spell != null && spellCooldowns.containsKey(spell)) {
			this.cooldowns.put(spell, this.tick + spellCooldowns.get(spell));
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	public Spell getSpell() {
		int i = this.player.getInventory().getHeldItemSlot();
		return getSpell(i);
	}

	public Spell getSpell(int slot) {
		return this.spells[slot];
	}

	@Override
	protected void handle() {
		Set<Spell> keys = new HashSet<>();
		keys.addAll(this.cooldowns.keySet());
		for (Spell type : keys) {
			if (this.tick > this.cooldowns.get(type)) {
				this.cooldowns.remove(type);
			}
		}

		if (this.tick > this.globalCooldown) {
			this.globalCooldown = 0;
		}

		List<Progression> progressionKeys = new ArrayList<>();
		progressionKeys.addAll(this.progressions);
		for (Progression progression : progressionKeys) {
			if (!progression.isAlive) {
				this.progressions.remove(progression);
			}
		}

	}

	@Override
	public void initialize() {

	}

	private boolean isReady(Spell spell) {
		return this.globalCooldown == 0 && !this.cooldowns.containsKey(spell);
	}

	@Override
	protected void login(PlayerLoginEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	protected int maxVotes() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void save() {

		// table creation
		// create table playerData (uuid varchar(40), slot0 varchar(40), slot1
		// varchar(40), slot2 varchar(40), slot3 varchar(40), slot4 varchar(40),
		// slot5 varchar(40), slot6 varchar(40), slot7 varchar(40), slot8
		// varchar(40));

		final StringBuilder statement = new StringBuilder();
		statement.append("UPDATE playerData SET ");

		for (int i = 0; i < 9; i++) {
			statement.append("slot");
			statement.append(i);
			statement.append("=");
			final Spell spell = this.spells[i];
			if (spell == null) {
				statement.append("null");
			} else {
				statement.append("'");
				statement.append(spell.name());
				statement.append("'");
			}
			if (i != 8) {
				statement.append(", ");
			}
		}
		statement.append(" WHERE uuid='");
		statement.append(this.playerUUID);
		statement.append("';");
		try {
			this.plugin.sqlHandler.update(statement.toString());
		} catch (final SQLException e) {
			e.printStackTrace();
		}

	}

	public void setSpell(int slot, Spell spell) {
		this.spells[slot] = spell;
		save();
	}

	@Override
	protected void turnOffSpectating() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void turnOnSpectating() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean useItem(int slot, ItemStack item, Action action) {
		if (action != Action.LEFT_CLICK_AIR && action != Action.LEFT_CLICK_BLOCK) {
			return false;
		}
		Spell spell = getSpell(slot);
		if (spell == null) {
			return false;
		}
		if (!this.player.hasPermission(spell.getPermissionNode())) {
			return false;
		}
		switch (spell) {
		case TELEPORT:
			if (isReady(spell)) {
				new Teleport(this);
				cooldown(spell);
			}
			break;
		case GUST:
			if (isReady(spell)) {
				new Gust(this);
				cooldown(spell);
			}
			break;
		case TORNADO:
			if (isReady(spell)) {
				new Tornado(this);
				cooldown(spell);
			}
			break;
		case SONIC_BOOM:
			if (isReady(spell)) {
				new SonicBoom(this);
				cooldown(spell);
			}
			break;
		case LIGHTNING:
			if (isReady(spell)) {
				new Lightning(this);
				cooldown(spell);
			}
			break;
		case INFERNO:
			if (isReady(spell)) {
				new Inferno(this);
				cooldown(spell);
			}
		default:
			break;

		}
		return false;
	}

}
