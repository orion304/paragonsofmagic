package main.java.org.valor.pom.main;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import main.java.org.valor.pom.schools.School;
import main.java.org.valor.pom.schools.Spell;
import src.main.java.org.orion304.menu.Menu;
import src.main.java.org.orion304.menu.MenuItem;

public class POMSpellMenu extends Menu {

	private static final MenuItem backItem = new MenuItem("Back", Material.REDSTONE, 1, "Back to schools");

	private final Spell[] spells;
	private final POMPlayer pPlayer;

	public POMSpellMenu(POMPlayer source, School school) {
		super(source.getPlugin(), "Paragons Of Magic", 18);
		pPlayer = source;

		List<Spell> spellList = Spell.getSpells(school);
		spells = new Spell[spellList.size()];

		int i = 0;
		Player player = pPlayer.getPlayer();
		for (Spell spell : spellList) {
			if (player.hasPermission(spell.getPermissionNode())) {
				spells[i] = spell;
				addMenuItem(i, spell.getMenuItem());
			} else {
				spells[i] = null;
			}
			i++;
		}

		addMenuItem(17, backItem);

		openMenu(player);

	}

	public void registerClick(int slot) {
		if (slot < spells.length) {
			pPlayer.getPlayer().closeInventory();
			new POMAssignSlotMenu(pPlayer, spells[slot]);
		} else if (slot == 17) {
			pPlayer.getPlayer().closeInventory();
			new POMSchoolMenu(pPlayer);
		}
	}

}
