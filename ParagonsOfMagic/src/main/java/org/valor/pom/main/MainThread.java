package main.java.org.valor.pom.main;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

import main.java.org.valor.pom.schools.Progression;

public class MainThread implements Runnable {

	private static final Map<LivingEntity, LivingEntity> fliers = new HashMap<>();

	public static void causeFlight(LivingEntity thrower, Entity flier) {
		if (flier instanceof LivingEntity) {
			fliers.put((LivingEntity) flier, thrower);
		}
	}

	public static LivingEntity getThrower(LivingEntity thrown) {
		return fliers.get(thrown);
	}

	private final ParagonsOfMagic plugin;

	private boolean broken = false;

	public MainThread(ParagonsOfMagic plugin) {
		this.plugin = plugin;
	}

	@Override
	public void run() {
		if (broken) {
			return;
		}

		try {
			Progression.handle();
		} catch (Exception e) {
			e.printStackTrace();
			broken = true;
		}

	}

}
