package main.java.org.valor.pom.main;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import main.java.org.valor.pom.schools.School;
import src.main.java.org.orion304.menu.Menu;
import src.main.java.org.orion304.menu.MenuItem;

public class POMSchoolMenu extends Menu {

	private static final MenuItem exitItem = new MenuItem(ChatColor.LIGHT_PURPLE + "Exit", Material.REDSTONE, 1,
			ChatColor.WHITE + "Leave this menu");

	private final School[] schools = new School[School.values().length];
	private final POMPlayer pPlayer;

	public POMSchoolMenu(POMPlayer source) {
		super(source.getPlugin(), "Paragons Of Magic", 18);
		this.pPlayer = source;
		int i = 0;
		for (School school : School.values()) {
			schools[i] = school;
			addMenuItem(i, school.getMenuItem());
			i++;
		}

		addMenuItem(17, exitItem);

		openMenu(source.getPlayer());
	}

	public void registerClick(int slot) {
		if (slot < schools.length) {
			School school = schools[slot];
			new POMSpellMenu(pPlayer, school);
		} else {
			pPlayer.getPlayer().closeInventory();
		}
	}

}
